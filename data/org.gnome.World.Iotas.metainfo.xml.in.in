<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2022 Chris Heywood -->
<component type="desktop">
  <id>@APPID@</id>
  <name translate="no">Iotas</name>
  <launchable type="desktop-id">@APPID@.desktop</launchable>
  <project_license>GPL-3.0-or-later</project_license>
  <metadata_license>CC0-1.0</metadata_license>
  <content_rating type="oars-1.1" />
  <!-- Translators: The application's summary / tagline -->
  <summary>Simple note taking</summary>

  <translation type="gettext">@GETTEXT_PACKAGE@</translation>

  <url type="homepage">https://gitlab.gnome.org/World/iotas</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/iotas/-/issues</url>

  <developer id="gnome.org">
      <name translate="no">Chris Heywood</name>
  </developer>

  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </recommends>

  <requires>
    <display_length compare="ge">360</display_length>
  </requires>

  <kudos>
      <!--
        GNOME Software kudos:
        https://gitlab.gnome.org/GNOME/gnome-software/blob/main/doc/kudos.md
      -->
      <kudo>ModernToolkit</kudo>
      <kudo>HiDpiIcon</kudo>
  </kudos>

  <custom>
    <value key="Purism::form_factor">mobile</value>
  </custom>

  <branding>
    <color type="primary" scheme_preference="light">#ffbfad</color>
    <color type="primary" scheme_preference="dark">#62342b</color>
  </branding>

  <description>
    <!-- Translators: Part of metainfo description. "Iotas" is the application name; do not translate. -->
    <p>Iotas aims to provide distraction-free note taking via its mobile-first design.</p>
    <!-- Translators: Part of metainfo description -->
    <p>Featuring</p>
    <ul>
      <!-- Translators: Part of metainfo description -->
      <li>Optional speedy sync with Nextcloud Notes</li>
      <!-- Translators: Part of metainfo description -->
      <li>Offline note editing, syncing when back online</li>
      <!-- Translators: Part of metainfo description -->
      <li>Category editing and filtering</li>
      <!-- Translators: Part of metainfo description -->
      <li>Favorites</li>
      <!-- Translators: Part of metainfo description -->
      <li>Spell checking</li>
      <!-- Translators: Part of metainfo description -->
      <li>Search within the collection or individual notes</li>
      <!-- Translators: Part of metainfo description -->
      <li>Focus mode and optional hiding of the editor header and formatting bars</li>
      <!-- Translators: Part of metainfo description -->
      <li>In preview: export to PDF, ODT and HTML</li>
      <!-- Translators: Part of metainfo description -->
      <li>A convergent design, seeing Iotas as at home on desktop as mobile</li>
      <!-- Translators: Part of metainfo description -->
      <li>Search from GNOME Shell</li>
      <!-- Translators: Part of metainfo description -->
      <li>Note backup and restoration (from CLI, for using without sync)</li>
      <!-- Translators: Part of metainfo description -->
      <li>The ability to change font size and toggle monospace style</li>
    </ul>
    <!-- Translators: Part of metainfo description -->
    <p>Writing in markdown is supported but optional, providing</p>
    <ul>
      <!-- Translators: Part of metainfo description -->
      <li>Formatting via toolbar and shortcuts</li>
      <!-- Translators: Part of metainfo description -->
      <li>Syntax highlighting with themes</li>
      <!-- Translators: Part of metainfo description -->
      <li>A formatted view</li>
      <!-- Translators: Part of metainfo description -->
      <li>The ability to check off task lists from the formatted view</li>
    </ul>
    <!-- Translators: Part of metainfo description -->
    <p>Slightly more technical details, for those into that type of thing</p>
    <ul>
      <!-- Translators: Part of metainfo description -->
      <li>Nextcloud Notes sync is via the REST API, not WebDAV, which makes it snappy</li>
      <!-- Translators: Part of metainfo description -->
      <li>There's basic sync conflict detection</li>
      <!-- Translators: Part of metainfo description -->
      <li>Notes are constantly saved</li>
      <!-- Translators: Part of metainfo description -->
      <li>Large note collections are partially loaded to quicken startup</li>
      <!-- Translators: Part of metainfo description -->
      <li>Notes are stored in SQLite, providing for fast search (FTS) without reinventing the wheel. Plain files can be retrieved by making a backup (CLI).</li>
    </ul>
  </description>

  <screenshots>
    <screenshot type="default">
      <!-- Translators: A screenshot description. -->
      <caption>Index</caption>
      <image>https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/desktop_index.png</image>
    </screenshot>
    <screenshot>
      <!-- Translators: A screenshot description. -->
      <caption>Editor with markdown</caption>
      <image>https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/desktop_editor_markdown.png</image>
    </screenshot>
    <screenshot>
      <!-- Translators: A screenshot description. -->
      <caption>Rendered markdown</caption>
      <image>https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/desktop_markdown_render.png</image>
    </screenshot>
    <screenshot>
      <!-- Translators: A screenshot description. -->
      <caption>Index in dark style</caption>
      <image>https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/desktop_index_dark.png</image>
    </screenshot>
    <screenshot>
      <!-- Translators: A screenshot description. -->
      <caption>Mobile</caption>
      <image>https://gitlab.gnome.org/World/iotas/-/raw/main/data/screenshots/mobile.png</image>
    </screenshot>
  </screenshots>

  <releases>
    <release version="0.10.2" date="2025-02-27T00:00:00Z">
      <description translate="no">
        <p>An attempted fix for false sync conflicts potentially resulting from response caching.</p>
      </description>
    </release>
    <release version="0.10.1" date="2025-02-19T00:00:00Z">
      <description translate="no">
        <p>This release brings the ability to switch back to the previous note (only via ctrl-L for now, L for last note), fixes a few input focus issues and squashes a bug for systems with no dictionaries.</p>
        <p>Contributors</p>
        <ul>
          <li>Balló György</li>
        </ul>
      </description>
    </release>
    <release version="0.10.0" date="2025-01-23T00:00:00Z">
      <description translate="no">
        <p>This release sees a larger set of design updates and other changes.</p>
        <p>Highlights</p>
        <ul>
            <li>When not pinned the index sidebar now behaves as an overlay</li>
            <li>Both the header and formatting bars in the editor are now overlays. This avoids having the note text move when the header bar hides.</li>
            <li>Using alt + left in the editor now moves the current word/selection left. This brings consistency with the other alt + arrow key shortcuts for moving text. Escape can be used to return to the index.</li>
            <li>High contrast appearance has been improved for the editor and markdown render. The default syntax theme (Monochrome) is recommended for high contrast.</li>
            <li>Ctrl-clicking links in the editor now opens them</li>
            <li>Updated translations: Brazilian Portuguese, Chinese, French, German and Occitan</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Arnaud Ferraris</li>
          <li>Brage Fuglseth</li>
          <li>Filipe Motta</li>
          <li>Irénée Thirion</li>
          <li>Jürgen Benvenuti</li>
          <li>Kramo</li>
          <li>lumingzh</li>
          <li>Quentin Pagès</li>
        </ul>
      </description>
    </release>
    <release version="0.9.5" date="2024-11-15T00:00:00Z">
      <description translate="no">
        <p>Improves unicode HTML exports</p>
      </description>
    </release>
    <release version="0.9.4" date="2024-10-29T00:00:00Z">
      <description translate="no">
        <p>Using the formatting toolbar no longer hides the keyboard on mobile.</p>
        <p>Contributors</p>
        <ul>
          <li>Guido Günther</li>
        </ul>
      </description>
    </release>
    <release version="0.9.3" date="2024-10-15T00:00:00Z">
      <description translate="no">
        <p>The strikethrough shortcut has been changed to Control-Shift-X and the Occitan translation has been updated.</p>
        <p>Contributors</p>
        <ul>
          <li>Quentin Pagès</li>
        </ul>
      </description>
    </release>
    <release version="0.9.2" date="2024-10-07T00:00:00Z">
      <description translate="no">
        <p>False sync conflicts on flaky connections are now avoided and text such as what_is_a_code_span will no longer wrongly invoke italics in the editor.</p>
      </description>
    </release>
    <release version="0.9.1" date="2024-09-25T00:00:00Z">
      <description translate="no">
        <p>Speed - switching between categories, switching in and out of search, and searching itself are all faster.</p>
        <p>Plus when inserting a new item in the middle of an ordered list the numbers for the remainder of the list are now incremented. See the git log for other minor changes.</p>
      </description>
    </release>
    <release version="0.9.0" date="2024-09-18T00:00:00Z">
      <description translate="no">
        <p>This release brings a first pass at adding formatting assistance via a toolbar and keyboard shortcuts.</p>
        <p>Other changes</p>
        <ul>
          <li>Fixed assisted edits not having atomic undo actions (eg. indenting multiple lines)</li>
          <li>Added margin below cursor for when appending at the bottom of the editor</li>
          <li>Excerpt generator updated to show list bullet points</li>
          <li>Switched to v47 SDK</li>
          <li>Brazilian Portuguese translation updated</li>
          <li>Chinese translation updated</li>
          <li>French translation updated</li>
          <li>German translation updated</li>
          <li>Other fixes (see git log)</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Filipe Motta</li>
          <li>Irénée Thirion</li>
          <li>Jürgen Benvenuti</li>
          <li>lumingzh</li>
        </ul>
      </description>
    </release>
    <release version="0.8.2" date="2024-08-08T00:00:00Z">
      <description translate="no">
        <p>Fixes issue editing while searching note, makes ctrl-f refocus editor search and updates Occitan translation.</p>
        <p>Contributors</p>
        <ul>
          <li>Quentin Pagès</li>
        </ul>
      </description>
    </release>
    <release version="0.8.1" date="2024-08-07T00:00:00Z">
      <description translate="no">
        <p>Three minor fixes: new notes no longer flag an extra sync after closing, the index button to show earlier notes no longer appears after adding a first note and math TeX equations now appear in HTML exports.</p>
      </description>
    </release>
    <release version="0.8.0" date="2024-06-06T00:00:00Z">
      <description translate="no">
        <p>With this release Iotas jumps to version v0.8 to better communicate its maturity. This doesn't reflect any grand changes; it's business as usual otherwise.</p>
        <ul>
          <li>Added ability to backup from CLI without closing open window</li>
          <li>Added keyboard shortcuts: F10 for menu and ctrl-w for closing window</li>
          <li>Improved sync onboarding with detection of missing Notes app and notification of generic SSL certificate issues</li>
          <li>Chinese translation added</li>
          <li>Brazilian Portuguese translation updated</li>
          <li>Czech translation updated</li>
          <li>French translation updated</li>
          <li>German translation updated</li>
          <li>Italian translation updated</li>
          <li>Occitan translation updated</li>
          <li>Turkish translation updated</li>
          <li>Other minor fixes (see git log)</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Daniele Verducci</li>
          <li>Filipe Motta</li>
          <li>Irénée Thirion</li>
          <li>Jiri Eischmann</li>
          <li>Jürgen Benvenuti</li>
          <li>lumingzh</li>
          <li>Quentin Pagès</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.2.14" date="2024-05-03T00:00:00Z">
      <description translate="no">
        <p>Changes</p>
        <ul>
          <li>Fixed crash in search</li>
          <li>Fixed non translatable text</li>
          <li>Brazilian Portuguese translation updated</li>
          <li>Turkish translation updated</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Filipe Motta</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.2.13" date="2024-04-19T00:00:00Z">
      <description translate="no">
        <p>Changes</p>
        <ul>
          <li>Applied timeouts to network requests</li>
          <li>Tweaked the editor line length limiting</li>
          <li>Fixed an ordering issue in the shell search provider</li>
          <li>Fixed a crash reauthenticating against Nextcloud</li>
          <li>Fixed the shell search provider not exiting when idle</li>
          <li>German translation updated</li>
          <li>Occitan translation updated</li>
          <li>Libraries updated</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Jürgen Benvenuti</li>
          <li>Quentin Pagès</li>
        </ul>
      </description>
    </release>
    <release version="0.2.12" date="2024-04-02T00:00:00Z">
      <description translate="no">
        <p>The biggest change in this release is faster search in the index.</p>
        <p>Editor search and replace paper cuts</p>
        <ul>
          <li>Restored automatically navigating to the first match</li>
          <li>Fixed ordering issue when replacing multiple times</li>
          <li>Fixed current match being lost opening replace</li>
          <li>Fixed bug with shortcut handling when already searching</li>
          <li>Fixed pressing enter not doing anything useful</li>
          <li>Fixed jumping between matches not always scrolling all the way to the match</li>
        </ul>
        <p>Other changes</p>
        <ul>
          <li>Added the ability to scroll or drag through the bottom of the index to show older notes</li>
          <li>Fixed first markdown render scroll position</li>
          <li>Fixed window slightly too wide on mobile</li>
          <li>Brazilian Portuguese translation updated</li>
          <li>Czech translation updated</li>
          <li>French translation updated</li>
          <li>German translation updated</li>
          <li>Italian translation updated</li>
          <li>Occitan translation updated</li>
          <li>Spanish translation updated</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Daniele Verducci</li>
          <li>Filipe Motta</li>
          <li>Irénée Thirion</li>
          <li>Jiri Eischmann</li>
          <li>Jürgen Benvenuti</li>
          <li>Óscar Fernández Díaz</li>
          <li>Quentin Pagès</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.2.11" date="2024-03-20T00:00:00Z">
      <description translate="no">
        <p>Changes</p>
        <ul>
          <li>Allowed typing while keyboard navigating the index to append the current search term</li>
          <li>Improved unsynced changes marker in editor</li>
          <li>Fixed flickering codeblocks in render view</li>
          <li>Fixed selections being left in index sections using tab</li>
          <li>Updated to libadwaita v1.5</li>
        </ul>
      </description>
    </release>
    <release version="0.2.10" date="2024-03-13T00:00:00Z">
      <description translate="no">
        <p>Bugfix release for text cursor issue found in v0.2.9</p>
      </description>
    </release>
    <release version="0.2.9" date="2024-03-09T00:00:00Z">
      <description translate="no">
        <p>New features</p>
        <ul>
          <li>Exporting to PDF, ODT, HTML (and MD). In preview, feedback encouraged.</li>
          <li>Focus mode</li>
        </ul>
        <p>Other changes</p>
        <ul>
          <li>Improved cold startup performance</li>
          <li>Made render view loading screen display earlier</li>
          <li>Fixed alerts being created working around a toolkit issue</li>
          <li>Italian translation added</li>
          <li>Brazilian Portuguese translation updated</li>
          <li>Czech translation updated</li>
          <li>French translation updated</li>
          <li>German translation updated</li>
          <li>Occitan translation updated</li>
          <li>Spanish translation updated</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Daniele Verducci</li>
          <li>Filipe Motta</li>
          <li>Irénée Thirion</li>
          <li>Jiri Eischmann</li>
          <li>Jürgen Benvenuti</li>
          <li>Óscar Fernández Díaz</li>
          <li>Quentin Pagès</li>
        </ul>
      </description>
    </release>
    <release version="0.2.8" date="2024-02-23T00:00:00Z">
      <description translate="no">
        <p>New features</p>
        <ul>
          <li>Search and replace in editor</li>
          <li>Automatic editor headerbar hiding</li>
          <li>Search within markdown render</li>
          <li>Basic extending of ordered lists</li>
          <li>Sidebar scaling for the desktop</li>
          <li>Feedback while loading render engine</li>
          <li>Ability to create a note from within another note via control N, bringing any selection along</li>
          <li>Keyboard shortcut to open first search result in index - alt enter</li>
        </ul>
        <p>Other changes</p>
        <ul>
          <li>Sync notifications in the index are now only shown when there are changes</li>
          <li>The shell search provider now closes when idle</li>
          <li>Updated and changed icons</li>
          <li>Build system tweaks</li>
          <li>Czech translation added</li>
          <li>Occitan translation added</li>
          <li>Brazilian Portuguese translation updated</li>
          <li>French translation updated</li>
          <li>German translation updated</li>
          <li>Spanish translation updated</li>
          <li>Turkish translation updated</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>David Lapshin</li>
          <li>Filipe Motta</li>
          <li>Hari Rana</li>
          <li>Irénée Thirion</li>
          <li>Jiri Eischmann</li>
          <li>Jürgen Benvenuti</li>
          <li>Nokse</li>
          <li>Óscar Fernández Díaz</li>
          <li>Quentin Pagès</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.2.7" date="2024-01-18T00:00:00Z">
      <description translate="no">
        <ul>
          <li>Changed unique identifier aligning with move into GNOME World</li>
          <li>Brazilian Portuguese translation added</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Filipe Motta</li>
        </ul>
      </description>
    </release>
    <release version="0.2.6" date="2023-10-31T00:00:00Z">
      <description translate="no">
        <ul>
          <li>Provided for restarting index search from search results (by keyboard)</li>
          <li>Fixed issue with editor scrolling for newlines</li>
          <li>Improved error handling during Nextcloud Notes sign in</li>
        </ul>
      </description>
    </release>
    <release version="0.2.5" date="2023-10-12T00:00:00Z">
      <description translate="no">
        <ul>
          <li>Added searching of notes from GNOME Shell</li>
          <li>Added adjustment of font size between fixed and proportional for the markdown render</li>
          <li>Tweaked the rendered task list style</li>
          <li>Resynced the editor margin background colours with Adwaita</li>
          <li>French translation updated</li>
          <li>German translation updated</li>
          <li>Turkish translation updated</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Irénée Thirion</li>
          <li>Jürgen Benvenuti</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.2.4" date="2023-09-21T00:00:00Z">
      <description translate="no">
        <p>The biggest change in this release is the ability to automatically expand the sidebar on desktop. The old behaviour has been retained as an option for the minimalists.</p>
        <p>There are a set of smaller changes</p>
        <ul>
          <li>Two cases which could result in false sync conflicts have been fixed</li>
          <li>Improvements for keyboard navigation in the sidebar</li>
          <li>Notifications are now shown for settings changed via keyboard shortcuts in the editor</li>
          <li>An issue has been fixed where the first search in a session could be very slow</li>
          <li>A bug where "Load Older Notes" would be shown in the index for tiny collections has been fixed</li>
          <li>An experiment has been started which, after opting in, provides an extended set of preferences</li>
          <li>Updated to GNOME platform v45 (and libadwaita v1.4 widgets)</li>
          <li>German translation updated</li>
          <li>Spanish translation updated</li>
          <li>Turkish translation updated</li>
        </ul>
        <p>Along with a few for the rendered markdown</p>
        <ul>
          <li>The system document font is now used</li>
          <li>Images no longer have to finish downloading before the view is shown</li>
          <li>Images wider than the window are now scaled to fit</li>
          <li>Whether notes are opened in edit or view mode can be now be configured (using an extended preference)</li>
          <li>Displaying with the system monospace font can be configured (using an extended preference)</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Jürgen Benvenuti</li>
          <li>Óscar Fernández Díaz</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.2.3" date="2023-09-04T00:00:00Z">
      <description translate="no">
        <ul>
          <li>Editor search match visibility has been improved and a match count is now shown</li>
          <li>Nextcloud Notes sign in has been tuned with it now waiting for initial note sync before continuing, the schema is pre-populated in the input box and an issue with initial filtering has been fixed</li>
          <li>Keyboard shortcuts have been added for</li>
          <li>.. Changing markdown list indentation (tab/shift-tab)</li>
          <li>.. Changing the editor content width (control up/down)</li>
          <li>.. Changing the editor font size (control minus/plus/zero)</li>
          <li>.. Flushing all context in the index (control delete/backspace)</li>
          <li>Link styling in the markdown render has been tweaked</li>
          <li>Issues with markdown list continuation have been fixed including poor copy/paste and undo/redo behaviour</li>
          <li>An index keyboard navigation bug has been fixed</li>
          <li>Buildsystem tweaks were made</li>
          <li>French translation updated</li>
          <li>German translation updated</li>
          <li>Spanish translation updated</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Arnaud Ferraris</li>
          <li>Irénée Thirion</li>
          <li>Jürgen Benvenuti</li>
          <li>Óscar Fernández Díaz</li>
        </ul>
      </description>
    </release>
    <release version="0.2.2" date="2023-08-01T00:00:00Z">
      <description translate="no">
        <ul>
          <li>Optional and experimental maths equation support has been added for markdown rendering</li>
          <li>Nextcloud Notes sync issues are more gracefully handled</li>
          <li>A crash entering search under specific conditions has been fixed</li>
          <li>The context-click menu in the markdown render has been tidied</li>
          <li>Python dependencies have been updated</li>
          <li>Updated German translation</li>
          <li>Updated Spanish translation</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Jürgen Benvenuti</li>
          <li>Óscar Fernández Díaz</li>
        </ul>
      </description>
    </release>
    <release version="0.2.1" date="2023-07-16T00:00:00Z">
      <description translate="no">
        <p>Bug fixes</p>
        <ul>
          <li>Adding a note after selecting a category in the sidebar no longer results in a crash</li>
        </ul>
      </description>
    </release>
    <release version="0.2.0" date="2023-07-15T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>If your mouse has a back button you can now use it to return to the index</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Category ordering has been fixed in the sidebar and category selection</li>
          <li>CRLF line breaks now display correctly in index excerpts</li>
          <li>Long pressing a word on mobile with spell checking disabled no longer results in a crash</li>
          <li>Disabling spellcheck works without having to restart</li>
        </ul>
        <p>Other changes</p>
        <ul>
          <li>Sizeable internal cleanup and translation improvement</li>
          <li>Updated French translation</li>
          <li>Updated German translation</li>
          <li>Updated Spanish translation</li>
          <li>Updated Turkish translation</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Irénée Thirion</li>
          <li>Jürgen Benvenuti</li>
          <li>Óscar Fernández Díaz</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.1.16" date="2023-05-08T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Added support for the read-only property from Nextcloud Notes</li>
          <li>Made backups use the file extension preference from the server</li>
          <li>Added some styling for tables in the markdown render</li>
        </ul>
        <p>Other changes</p>
        <ul>
          <li>Added a Nextcloud Notes API version check</li>
          <li>Improved naming of action to toggle rendered markdown view</li>
          <li>Server offline indicator changed to a banner</li>
          <li>Updated French translation</li>
          <li>Updated Turkish translation</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Irénée Thirion</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.1.15" date="2023-05-03T00:00:00Z">
      <description translate="no">
        <p>Bug fixes</p>
        <ul>
          <li>Fixed markdown render tasklist display issues (including checked status in subtasks and extra spacing)</li>
        </ul>
      </description>
    </release>
    <release version="0.1.14" date="2023-04-30T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Added accessing spelling suggestions via long touch on mobile</li>
          <li>Improved inline code block appearance in lists</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Fixed rendering of elements in task list items (eg. links and inline code blocks)</li>
          <li>Fixed long lines in code blocks overflowing their container in the rendered markdown</li>
        </ul>
        <p>Other changes</p>
        <ul>
          <li>Changed default to retaining WebKitGTK in memory between uses</li>
          <li>Updated to GNOME platform v44</li>
        </ul>
      </description>
    </release>
    <release version="0.1.13" date="2023-04-11T00:00:00Z">
      <description translate="no">
        <p>Bug fixes</p>
        <ul>
          <li>Resolved crash deleting note from editor</li>
        </ul>
      </description>
    </release>
    <release version="0.1.12" date="2023-04-05T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Notes can now be selected and actioned from the list</li>
          <li>Categories can be changed from the note list</li>
          <li>The note list can now be searched by simply starting typing</li>
          <li>Updated French translation</li>
          <li>Updated Spanish translation</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>If the keyring has to be manually unlocked sync will now start its regular updates afterwards</li>
          <li>Attempting to launch a second instance, from eg. the CLI, no longer breaks the first</li>
          <li>On mobile selected row borders in the note list now match their section borders</li>
          <li>Fix sidebar top level categories sometimes incorrectly being identified as sub-categories</li>
          <li>Resolved always appearing to update to v0.1.11</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Irénée Thirion</li>
          <li>Óscar Fernández Díaz</li>
        </ul>
      </description>
    </release>
    <release version="0.1.11" date="2023-03-28T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Updated sidebar to provide tree view when sub-categories are in use</li>
          <li>Added category labels in note list</li>
          <li>Added basic support for servers using self-signed certificates</li>
          <li>Provided for keyboard navigation in sidebar</li>
          <li>Improved filtering in category dropdown (no longer only matching from the start)</li>
          <li>Substituted markdown check marks with special characters in note excerpts</li>
          <li>Removed workaround when displaying the note list popup on touch devices</li>
          <li>Switched base language to American English</li>
          <li>Added British English translation</li>
          <li>Updated French translation</li>
          <li>Updated German translation</li>
          <li>Updated Spanish translation</li>
          <li>Updated Turkish translation</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Notes no longer lose the category they're created in when logged in</li>
          <li>Text selection now easier on touch devices, at the cost of not being able to swipe back to the note list</li>
          <li>Resolved crash on platforms not providing the FreeDesktop.org setting via D-Bus</li>
          <li>Refresh menu item now only shown when logged in</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Irénée Thirion</li>
          <li>Jürgen Benvenuti</li>
          <li>Óscar Fernández Díaz</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.1.10" date="2023-03-09T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Added initial support for categories (with more coming soon)</li>
          <li>Added backup and restoration (via the CLI, for when not signed into Nextcloud)</li>
          <li>Added reintegration of titles and categories sanitised by the server</li>
          <li>Added local sanitisation of titles</li>
          <li>Improved preferences layout</li>
          <li>Updated French translation</li>
          <li>Updated Spanish translation</li>
          <li>Updated Turkish translation</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Icon background no longer black under KDE</li>
          <li>Nextcloud signout now hidden when sync not established</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Irénée Thirion</li>
          <li>Óscar Fernández Díaz</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.1.9" date="2023-02-15T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Added searching within notes</li>
          <li>Added a simple fullscreen mode (keyboard-only access for now)</li>
          <li>Added the ability to hide the headerbar in the editor for minimised view (keyboard-only access for now)</li>
          <li>Added preference to toggle monospace font usage</li>
          <li>Added Dutch translation</li>
          <li>Updated French translation</li>
          <li>Updated German translation</li>
          <li>Updated Spanish translation</li>
          <li>Updated Turkish translation</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Cursor is now placed at the beginning of the loaded note</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Heimen Stoffels</li>
          <li>Irénée Thirion</li>
          <li>Jürgen Benvenuti</li>
          <li>Óscar Fernández Díaz</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.1.8" date="2023-02-02T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Updated about dialog using Libadwaita</li>
          <li>Made transition between mobile and desktop layouts in index animate</li>
          <li>Added French translation</li>
          <li>Updated German translation</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Ensured horizontal margin is retained in index at transition point between mobile and desktop layouts</li>
          <li>Changed to correct transition type between index and editor</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Adrien Plazas</li>
          <li>Irénée Thirion</li>
          <li>Jürgen Benvenuti</li>
        </ul>
      </description>
    </release>
    <release version="0.1.7" date="2022-11-10T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Font family for editor synced to system monospace font</li>
          <li>Updated Spanish translation</li>
          <li>Updated Turkish translation</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Óscar Fernández Díaz</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.1.6" date="2022-11-07T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Added a command line argument to create a new note</li>
          <li>Accommodate for markdown headings when syncing first line of note into title</li>
          <li>Avoid checking spelling inside markdown code elements and links</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Under Phosh the keyboard was appearing upon returning to the index from the markdown render view</li>
          <li>Crash extending markdown lists containing links</li>
        </ul>
      </description>
    </release>
    <release version="0.1.5" date="2022-11-02T00:00:00Z">
      <description translate="no">
        <p>This release adds initial markdown support</p>
        <ul>
          <li>Rendered markdown view</li>
          <li>Syntax highlighting</li>
          <li>Syntax themes</li>
          <li>Task list support including toggling items from rendered view (which directly sync to server)</li>
          <li>Scroll position matching between views (approximate)</li>
          <li>Changes from server update into rendered view</li>
          <li>Ability to disable markdown features</li>
        </ul>
        <p>Other features</p>
        <ul>
          <li>Support additional markers for list continuation</li>
          <li>Nextcloud Notes API logging improvements, custom user agent</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Note could be shown twice in index</li>
        </ul>
      </description>
    </release>
    <release version="0.1.4" date="2022-08-11T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Enable startup notification</li>
          <li>Added German translation</li>
          <li>Added Turkish translation</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Copy and paste merging problem with spelling enabled</li>
          <li>Task lists not continuing after checked task</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Jürgen Benvenuti</li>
          <li>Sabri Ünal</li>
        </ul>
      </description>
    </release>
    <release version="0.1.3" date="2022-08-11T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Introduced basic spell checking</li>
          <li>Updated icon</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Sam Hewitt</li>
        </ul>
      </description>
    </release>
    <release version="0.1.2" date="2022-07-28T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Allow basic keyboard navigation in index</li>
          <li>Provide direction for missing Secret Service</li>
          <li>Added Russian translation</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Fix case where no notes were shown on initial sync. from server if all notes fell into "the rest" (older than the preceding month)</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Yaroslav Pronin</li>
        </ul>
      </description>
    </release>
    <release version="0.1.1" date="2022-04-10T00:00:00Z">
      <description translate="no">
        <p>Features</p>
        <ul>
          <li>Continue basic markdown lists in notes</li>
          <li>Added Spanish translation</li>
        </ul>
        <p>Bug fixes</p>
        <ul>
          <li>Fix crash in sync RFC2822 date handling</li>
          <li>Fix title edits not getting immediately synced</li>
        </ul>
        <p>Contributors</p>
        <ul>
          <li>Óscar Fernández Díaz</li>
        </ul>
      </description>
    </release>
    <release version="0.1" date="2022-04-03T00:00:00Z">
      <description translate="no">
        <p>Initial release.</p>
      </description>
    </release>
  </releases>
</component>
